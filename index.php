<head>
<meta name="google-site-verification" content="m8eL6A8gd0-m68AtXY2M_aqXDfzYDGBexQxNu3Fh1y0" />
</head>
<?php
	include_once "common/base.php";
	$pageTitle = "Home";
	include_once "common/header.php";
?>

		<div id="main">
			<noscript>This site just doesn't work, period, without JavaScript</noscript>
<?php
if(isset($_SESSION['LoggedIn']) && isset($_SESSION['Username'])): 	            
	echo "\t\t\t<ul id=\"list\">\n";

	include_once 'inc/class.lists.inc.php';
	$lists = new ListzListsItems($db);
	list($LID, $URL, $order) = $lists->loadListItemsByUser();

	echo "\t\t\t</ul>";
?>

			<br />

			<form action="db-interaction/lists.php" id="add-new" method="post">
				<input type="text" id="new-list-item-text" name="new-list-item-text" />

				<input type="hidden" id="current-list" name="current-list" value="<?php echo $LID; ?>" />
				<input type="hidden" id="new-list-item-position" name="new-list-item-position" value="<?php echo ++$order; ?>" />

				<input type="image" name="add" src="images/add.png" id="addlistz" onmouseover="this.src='images/addhover.png'" onmouseout="this.src='images/add.png'" />
				<input type="hidden" name="token" id="token"
					value="<?php echo $_SESSION['token']; ?>" />
			</form>

			<div class="clear"></div>

			<div id="share-area">
				<p>Public list URL: <a target="_blank" href="http://epiclistz.com/<?php echo $URL ?>.html">http://epiclistz.com/<?php echo $URL ?>.html</a></p>
			</div>

			<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
			<script type="text/javascript" src="js/jquery.jeditable.mini.js"></script>
			<script type="text/javascript" src="js/lists.js"></script>
			<script type="text/javascript">
			         initialize();
			</script>

<?php
elseif(isset($_GET['list'])): 	            
	echo "\t\t\t<ul id='list'>\n";

	include_once 'inc/class.lists.inc.php';
	$lists = new ListzListsItems($db);
	list($LID, $URL) = $lists->loadListItemsByListId();

	echo "\t\t\t</ul>";
else:
?>
		  	      
			<img id="bg" src="/images/bg.png" alt="Your new list here!" />
                  
<?php endif; ?>
              		      		  
		</div>
		  
<?php
	
	include_once "common/close.php";
?>