<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"> 

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />  
	<meta name="google-site-verification" content="m8eL6A8gd0-m68AtXY2M_aqXDfzYDGBexQxNu3Fh1y0" />
	<title>epicListz | <?php echo $pageTitle ?></title>

	<link rel="stylesheet" href="style.css" type="text/css" />
	<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

	<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script>
</head>

<body>
	<a id="logo" href="http://epiclistz.com/"><img src="images/logo.png"/></a>
	<div id="page-wrap">
		<div id="header">
			<h1><a href="/">epicListz</a></h1>
			<div id="control">

<?php 
	if(isset($_SESSION['LoggedIn']) && isset($_SESSION['Username'])
		&& $_SESSION['LoggedIn']==1):
?>
				<p><a href="/logout.php" class="logout"></a> <a href="/account.php" class="account"></a> <a href="/index.php" class="home"></a></p>
<?php else: ?>
				<p><a href="/index.php" class="home"></a> <a href="/signup.php" id="signup"></a> <a id="login" href="/login.php"></a></p>
<?php endif; ?>

			</div>

			<div class="clear"></div>
		<div id="feedback"><a id="feedbacklink" href="feedback.php"><img src="images/feedback.png"></a></div>
		</div>
